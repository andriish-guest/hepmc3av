#ifdef __CINT__

#pragma link C++ struct HepMC3::GenEventData+;
#pragma link C++ struct HepMC3::GenRunInfoData+;
#pragma link C++ struct HepMC3::GenParticleData+;
#pragma link C++ struct HepMC3::GenVertexData+;
#pragma link C++ class std::vector<HepMC3::GenParticleData>+;
#pragma link C++ class std::vector<HepMC3::GenVertexData>+;
#pragma link C++ class std::vector<int>+;
#pragma link C++ class std::vector<std::string>+;
#pragma link C++ class HepMC3::FourVector+;
#pragma link C++ class HepMC3::Units+;
#pragma link C++ class HepMC3::Setup+;

/* To generate dictionaries for compatibility with HepMC3.0*/
#pragma link C++ struct HepMC::GenEventData+;
#pragma link C++ struct HepMC::GenRunInfoData+;
#pragma link C++ struct HepMC::GenParticleData+;
#pragma link C++ struct HepMC::GenVertexData+;
#pragma link C++ class std::vector<HepMC::GenParticleData>+;
#pragma link C++ class std::vector<HepMC::GenVertexData>+;
#pragma link C++ class HepMC::FourVector+;
#pragma link C++ class HepMC::Units+;
#pragma link C++ class HepMC::Setup+;



#endif
